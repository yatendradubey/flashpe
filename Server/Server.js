const express = require('express');
const bodyParser = require('body-parser');

const user = require('./routes/user');
const wallet = require('./routes/wallet');
const transaction = require('./routes/transaction');

const app = express();
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods","GET,POST,PUT,DELETE");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });


app.use(bodyParser.json()); 
app.use(bodyParser.urlencoded({extended: true}));

app.use(user);
app.use(wallet);
app.use(transaction);

app.listen(3000,'0.0.0.0',() => {
    console.log('server started on port 3000');
});
