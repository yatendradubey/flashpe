const mysql = require('mysql')

function connect(){
    var connection = mysql.createConnection({
        host:   'localhost',
        user:   'yaten',
        password: 'manager',
        database: 'flashpe_db',
        multipleStatements: true 
    });
    connection.connect();
    return connection;
}

module.exports = {
    connect: connect
}