const express = require('express')
const db = require('../common/database')
const utils = require('../common/utils')
const crypto = require('crypto-js')

var router = express.Router();


router.get('/user/checkBalance/:id', (request, response) => {
    const userId = request.params.id;
    var connection = db.connect();
    const statement = `select * from Account where userId = ${userId}`;
    connection.query(statement, (error, results) => {
        connection.end();
        response.send(utils.createResult(error, results));
       // console.log(results);
    });
})

router.get('/user/getCard/:id', (request,response) => {
    const userId = request.params.id;
   // console.log(userId);
    var connection = db.connect();

    const statement = `SELECT BankName, cardNumber FROM Account
                        RIGHT JOIN savedCards ON Account.userId = savedCards.userId
                        where Account.userId = '${userId}';`;

    //console.log(statement);
    connection.query(statement, (error,results) =>{
        console.log(results)
        connection.end();
        response.send(utils.createResult(error,results));
    });
})

router.post('/user/addCard/:id', (request,response) => {
    const userId = request.params.id;
    const cardNumber = request.body.cardNumber;
    const expiryMonth = request.body.expiryMonth;
    const expiryYear = request.body.expiryYear;
    const cvv = request.body.cvv;

   // console.log(request.body);
    var connection = db.connect();

    const statement = `INSERT INTO savedCards (cardNumber,expiryMonth,expiryYear,cvv,userId) VALUES('${cardNumber}','${expiryMonth}','${expiryYear}','${cvv}','${userId}');`;

   // console.log(statement)
    connection.query(statement, (error,results) =>{
        //console.log(results)
        connection.end();
        response.send(utils.createResult(error,results));
    });
})


router.get('/user/checkCVV/:id', (request,response) => {
    const userId = request.params.id;
    const cvv = request.body.cvv;
    const cardNumber = parseInt(request.body.cardNumber);
    console.log(cardNumber);
    //console.log(userId);
    //console.log(cvv);
    var connection = db.connect();

    const statement = `SELECT cvv from savedCards where userId = '${userId}' AND cardNumber = '${cardNumber}';`;

    //console.log(statement);
    connection.query(statement, (error,results) =>{
       // console.log(results);
        //console.log(results[0].cvv);
        var result = {};
        if(results.length==0){
            result['status'] = 'error';
            result['message'] = 'Invalid card cvv. Please try again';
            response.send(result);
            connection.end();
        }else{
            const cardCvv = results[0].cvv;
          //  console.log(cardCvv);
        //  console.log(cvv); 
            if(cardCvv != cvv){
                result['status'] = 'error';
                result['message'] = 'Invalid card cvv';
            }else{
                result['status'] = 'success';
                result['data'] = cardCvv;
            }
            response.send(result);
            connection.end();
            console.log(result);
        }
        //connection.end();
        //response.send(utils.createResult(error,results));
    });
})

router.put('/user/addMoneyToWallet/:id', (request,response) => {
    const userId = request.params.id;
    const amountToBeAdded = request.body.WTAmount;
   // console.log(request.body);
   // console.log(amountToBeAdded);
    var walletMoney;
    var bankBalance;
    var connection = db.connect();

    const statement1 = `select * from Account where userId = ${userId}`;
    connection.query(statement1,(error,results) => {
        walletMoney = results[0].walletMoney;
       // console.log(walletMoney);
    });
    const statement2 = `select * from Account where userId = ${userId}`;
    connection.query(statement2,(error,results) => {
        bankBalance = results[0].Amount;
        console.log("bankBalance:    "+bankBalance);
    });
    setTimeout(()=>{
        console.log(walletMoney);
        netWalletAmount = parseInt(walletMoney) + parseInt(amountToBeAdded);
        netAcountBalance = parseInt(bankBalance) - parseInt(amountToBeAdded);
        console.log("Net wallet Amount:    "+netWalletAmount);
        console.log("net account balance:   "+netAcountBalance);
        const statement = `UPDATE Account set walletMoney = '${netWalletAmount}' where userId = '${userId}';
                            UPDATE Account set Amount = '${netAcountBalance}' where userId = '${userId}'`;
    
        console.log(statement)
        connection.query(statement,[2,1], (error,results) =>{
            console.log(results)
            connection.end();
            response.send(utils.createResult(error,results));
        });
    },1000);
    
})

router.put('/user/sendWalletMoney/:id',(request,response) => {
    const userId = request.params.id;
    const receiverPhoneNumber = request.body.receiverPhoneNumber;
    const sendAmount = request.body.sendAmount;
   // console.log(request.body);
   // console.log(userId,receiverPhoneNumber,sendAmount);

    var connection = db.connect();
    var senderWalletMoney,receiverAccountMoney;

    const statement1 = `Select walletMoney from Account where userId = '${userId}'`;
    connection.query(statement1,(error,results) =>{
        console.log(results[0].Amount);
        senderWalletMoney= results[0].walletMoney;
    });

    const statement2 = `Select Amount from Account where phone = '${receiverPhoneNumber}'`;
    //console.log(statement2);
    connection.query(statement2,(error,results) =>{
      //  console.log("Statement1 results:    "+results[0].Amount);
        receiverAccountMoney= results[0].Amount;
    });

    setTimeout(()=>{
        console.log("sendAmount:    "+sendAmount);
        console.log("senderWalletMoney:   "+senderWalletMoney);
        console.log("receiverAccountMoney:     "+receiverAccountMoney);

        netSenderWalletMoney = senderWalletMoney - sendAmount;
        netReceiveAccountrMoney = receiverAccountMoney + parseInt(sendAmount);
        console.log("netSenderMoney:   "+netSenderWalletMoney);
        console.log("netReceiverMoney:     "+netReceiveAccountrMoney);

        const statement = `Update Account set walletMoney = '${netSenderWalletMoney}' where userId = '${userId}';
                           Update Account set Amount = '${netReceiveAccountrMoney}' where phone = '${receiverPhoneNumber}'`;
    
        connection.query(statement,[2,1],(error,results) => {
            console.log(statement)
            console.log(results);
            connection.end();
            response.send(utils.createResult(error,results));
        });

    },500);

});





module.exports = router