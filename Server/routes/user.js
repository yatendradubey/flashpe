const express = require('express')
const db = require('../common/database')
const utils = require('../common/utils')
const crypto = require('crypto-js')

var router = express.Router();

router.post('/user/login', (request, response) => {
    
    const email = request.body.email;
    const password = request.body.password;
  
    console.log(email,password);
    var connection = db.connect();
    const statement = `select id, firstName, lastName, email, isActive from users where email = '${email}' and password = '${password}'`;
    connection.query(statement, (error, results) => {
       // console.log(statement);
        //console.log(results);
        
        var result = {};
        if (results.length == 0) {
            result['status'] = 'error';
            result['message'] = 'Invalid email or password';
            response.send(result);
            connection.end();
            console.log(result);
        } else {
            const user = results[0];
            if (user.isActive == 0) {
                result['status'] = 'error';
                result['message'] = 'Your account is not actived yet..';
            } else {
                result['status'] = 'success';
                result['data'] = user;
            }
            connection.end();
            response.send(result);
        }
        
    });
})

router.post('/user/register', (request,response) => {
    const firstName = request.body.firstName
    const lastName = request.body.lastName
    const email = request.body.email
    const password = request.body.password
    const phone = request.body.phone
    const birthDate = request.body.birthDate
    const defaultIsActive = 0

    var connection = db.connect();

    const statement = `INSERT INTO users (firstName, lastName, email, password, isActive, createdTimestamp) 
                        VALUES ('${firstName}', '${lastName}', '${email}', '${password}', '${defaultIsActive}', CURRENT_TIMESTAMP);`;

    console.log(statement)
    connection.query(statement, (error,results) =>{
        console.log(results)
        connection.end();
        response.send(utils.createResult(error,results));
    });
})

router.put('/user/addAccount/:id', (request, response) => {
    
    const phone = request.body.phone;
    const userId = request.params.id;
    console.log(userId);
    console.log(phone);
    var connection = db.connect();
    const statement = `select id, AccountHolderName, phone from Account where phone = '${phone}';
                        UPDATE Account SET userId = '${userId}' WHERE phone = '${phone}';`;
    connection.query(statement,[2,1], (error, results) => {
        // console.log(statement);
        // console.log("Result[0]:  "+results[0]);
        // console.log("Result[1]:  "+results[1]);
        
        var result = {};
        if (results[0].length == 0) {
            result['status'] = 'error';
            result['message'] = 'Invalid Account.. Please Enter the phone number linked with a bank account';
            response.send(result);
            connection.end();
            console.log(result);
        } else {
                const AccountUser = results[0];
                result['status'] = 'success';
                result['data'] = AccountUser;
                connection.end();
                console.log(result);
            response.send(result);
            }
            
    
    });
})

router.put('/upi/:id', (request, response) => {
    const userId = request.params.id;
    console.log(request.body);
    const upi = request.body.upi;
    console.log(upi);
    var connection = db.connect();
    const statement = `update Account set upi = '${upi}@flashPe' where userId = ${userId}`;
    connection.query(statement, (error, results) => {
        console.log(statement);
        console.log(results);
       // console.log(response.body);
        connection.end();
        response.send(utils.createResult(error, results));

    });
})

router.put('/user/sendMoney/:id/', (request, response) => {
    const senderId = request.params.id;
    const sendAmount = request.body.sendAmount;
    const tempReceiverUPI = request.body.receiverUPI;
    const receiverUPI = tempReceiverUPI + "@flashPe";

    var senderAmount;
    var receiverAmount;
    var senderUPI;

    //console.log("senderId:  "+senderId);
    //console.log("receiverUPI:    "+receiverUPI);

    var connection = db.connect();
    
    const statement1 = `select * from Account where userId = ${senderId}`;
    connection.query(statement1,(error,results) => {
        senderAmount = results[0].Amount;
    });

    const statement2 = `select * from Account where upi = '${receiverUPI}'`;
    connection.query(statement2,(error,results) => {
        receiverAmount = results[0].Amount;
        var result = {}
        // if(results[0].upi=="7568974472@flashPe"){
        //     receiverAmount = receiverAmount - sendAmount;
        //     result['status'] = 'error';      
        // }
    });

    const statement3 = `select * from Account where userId = ${senderId}`;
    connection.query(statement3,(error,results) => {
        senderUPI = results[0].upi;
        console.log("senders UPI:   "+senderUPI);
    });

            setTimeout(()=>{
                if(senderUPI!=receiverUPI){
                    if(senderAmount>sendAmount){
                        ReceiverNetAmount = receiverAmount + parseInt(sendAmount); 
                        SenderNetAmount = senderAmount - sendAmount;
                    
                        const statement = `update Account set Amount = '${SenderNetAmount}' where userId = ${senderId};
                                           update Account set Amount = '${ReceiverNetAmount}' where upi = '${receiverUPI}';`;
                    
                        connection.query(statement,[2,1],(error,results) =>{
                       // console.log(results[0].fieldCount);
                        connection.end();
                        response.send(utils.createResult(error,results));
                        }); 
                    }else{
                        var result = {};  
                        result['status'] = 'error';
                        result['message'] = `INSUFFICIENT FUNDS IN YOUR ACCOUNT`;
                        response.send(result);
                        connection.end();
                        console.log(result);
                    }
                }else{
                    var result = {};  
                    result['status'] = 'error';
                    result['message'] = `RECEIVER'S UPI ID CANNOT BE SAME AS YOUR UPI ID`;
                    response.send(result);
                    connection.end();
                    console.log(result);
                }     
        },500)
       
});



router.get('/bankAccount/:id', (request, response) => {
    const id = request.params.id;
  //  console.log(id)
    var connection = db.connect();
    const statement = `select id, accountHolderName, upi, BankName, Branch from Account where userId = ${id}`;
    connection.query(statement, (error, results) => {
        connection.end();
        response.send(utils.createResult(error, results));                                                                                                                                                                                                          
       console.log(results);
    });
})

router.put('/user/createUpiPin/:id',(request, response) =>{
    const id = request.params.id;
    const upiPin = request.body.upiPin;
    console.log(id);
    console.log(upiPin);
    var connection = db.connect();
    const statement = `update Account set UpiPin = '${upiPin}' where userId = '${id}'`
    connection.query(statement,(error,results)=>{
        connection.end();
        response.send(utils.createResult(error,results));
        console.log(results);
    });
});
router.post('/user/checkUpiPin/:id',(request, response) =>{
    const id = request.params.id;
    const upiPin = request.body.upiPin;
  //  console.log(id);
  //  console.log(upiPin);
    var connection = db.connect();
    const statement = `select * from Account where UpiPin = '${upiPin}' and userId = '${id}'`
    connection.query(statement,(error,results)=>{
        var result = {};
        if (results.length == 0) {
            result['status'] = 'error';
            result['message'] = 'WRONG UPI PIN... PLEASE TRY AGAIN';
            response.send(result);
            connection.end();
            console.log(result);
        } else {
            const user = results[0];
                result['status'] = 'success';
                result['data'] = user;
            
            connection.end();
            response.send(result);
        }
    });
});
router.get('/user/checkUpiId/:id',(request, response) =>{
    const id = request.params.id;
    const upiId = request.body.upiId+"@flashPe";
  //  console.log(id);
   // console.log(upiId);
    var connection = db.connect();
    const statement = `select * from Account where upi = '${upiId}'`;
  //  console.log(statement);
    connection.query(statement,(error,results)=>{
        var result = {};
        if (results.length == 0) {
            result['status'] = 'error';
            result['message'] = 'INVALID UPI ID';
            response.send(result);
            connection.end();
            console.log(result);
        } else {
            const user = results[0];
                result['status'] = 'success';
                result['data'] = user;
            
            connection.end();
            response.send(result);
        }
    });
});


router.post('/admin/login', (request, response) => {
    const userName = request.body.userName;
    const password = request.body.password;

    var connection = db.connect();
    const statement = `select id, userName from admin where userName = '${userName}' and password = '${password}'`;
    console.log(statement);

    connection.query(statement, (error, results) => {
        connection.end();
        var result = {};
        if (results.length === 0) {
            result['status'] = 'error';
            result['message'] = 'invalid user name or password';
        } else {
            result['status'] = 'success';
            result['data'] = results;
        }
        response.send(result);
    });
});
router.get('/user/getlist', (request, response) => {
    var connection = db.connect();
    const statement = `select * from users`;
    connection.query(statement, (error, results) => {
        connection.end();
        response.send(utils.createResult(error, results));
        console.log(results);
    });
})

router.put('/user/getlist/status/:id', (request, response) => {
    const customerId = request.params.id;
    const status = request.body.status; // 0 or 1
    console.log("HI");
    var connection = db.connect();
    const statement = `update users set isActive = ${status} where id = ${customerId}`;
    connection.query(statement, (error, results) => {
        connection.end();
        response.send(utils.createResult(error, results));
    });
})

module.exports = router
