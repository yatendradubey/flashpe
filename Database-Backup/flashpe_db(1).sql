-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 30, 2019 at 08:49 AM
-- Server version: 5.7.24
-- PHP Version: 7.2.10-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `flashpe_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `Account`
--

CREATE TABLE `Account` (
  `id` int(11) NOT NULL,
  `AccountHolderName` varchar(255) DEFAULT NULL,
  `AccountNumber` bigint(21) DEFAULT NULL,
  `upi` varchar(21) DEFAULT NULL,
  `UpiPin` int(11) DEFAULT NULL,
  `phone` bigint(21) DEFAULT NULL,
  `Amount` double(21,2) DEFAULT NULL,
  `userId` int(11) DEFAULT NULL,
  `BankName` varchar(100) DEFAULT NULL,
  `Branch` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Account`
--

INSERT INTO `Account` (`id`, `AccountHolderName`, `AccountNumber`, `upi`, `UpiPin`, `phone`, `Amount`, `userId`, `BankName`, `Branch`) VALUES
(1, 'Yatendra Dubey', 7193629348173921, '7568974472@flashPe', 1231, 7568974472, 10100.00, 57, 'HDFC BANK', 'Surya Nagar'),
(2, 'Manoj Dubey', 7891201202201021, '9997558794@flashPe', NULL, 9997558794, 1100.00, 72, 'Punjab National Bank', 'Hinjewadi'),
(3, 'Raju Sharma', 78123456789077645, '', NULL, 1231231231, 2380000.00, NULL, 'ICICI', 'Viman Nagar'),
(4, 'Tanuja Dubey', 7899120120227771, '8909691271@flashPe', NULL, 8909691271, 2900.00, NULL, 'SBI Bank', 'Pratap Nagar'),
(5, 'Yat', 7374233295293592, '7568974473@flashPe', NULL, 7568974471, 1800.00, 77, 'Bank of Baroda', 'Vigyan Nagar'),
(9, 'qwer', 123, NULL, NULL, 7568974479, 1000.00, 77, 'qwe', 'rew'),
(10, 'kiran', 7374233295293591, '7568974473@flashPe', NULL, 7568974473, 5000.00, 78, 'HDFC', 'Arjun nagar'),
(11, 'Kundan', 7374233295293524, '9527974929@flashPe', NULL, 9527974929, 10000.00, 79, 'HDFC', 'Market  Yard'),
(12, 'Kiran', 23124, '1215@flashPe', 1215, 9823751872, 9900.00, 81, 'HDFC', 'Market Yard'),
(13, 'Abhishek', 7374233295293511, '1234512345@flashPe', 1234, 1234512345, 2000.00, 82, 'HDFC', 'Arjun Nagar'),
(14, 'pritesh', 7374233295293598, '9405193435@flashPe', 1234, 9405193435, 9900.00, 83, 'ICICI BANK', 'Arjun Nagar');

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `userName` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `userName`, `password`) VALUES
(1, 'admin', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `firstName` varchar(255) DEFAULT NULL,
  `lastName` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `isActive` int(11) DEFAULT NULL,
  `profileImage` blob,
  `createdTimestamp` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `firstName`, `lastName`, `email`, `password`, `phone`, `age`, `gender`, `isActive`, `profileImage`, `createdTimestamp`) VALUES
(57, 'Yaten', 'Dubey', 'y@g.com', '123', NULL, NULL, NULL, 1, NULL, '2018-12-23 15:43:15'),
(58, 'vivek', 'dubey', 'viek@g.com', '123', NULL, NULL, NULL, 0, NULL, '2018-12-23 15:50:31'),
(59, 'Ram', 'Singh', 'ram@g.com', '123', NULL, NULL, NULL, 0, NULL, '2018-12-23 15:51:52'),
(60, 'Johgjhgj', 'dubey', 'john@g.com', 'qwer', NULL, NULL, NULL, 0, NULL, '2018-12-23 16:00:28'),
(61, 'Shubham', 'Naidu', 'shubham@g.com', '123', NULL, NULL, NULL, 1, NULL, '2018-12-24 07:43:37'),
(62, 'shivangi', 'kalra', 'shiv@g.com', '123', NULL, NULL, NULL, 0, NULL, '2018-12-24 18:56:14'),
(63, 'Rupal', 'Gupta', 'rupal@g.com', '123', NULL, NULL, NULL, 0, NULL, '2018-12-24 19:05:48'),
(64, 'prachi', 'tomar', 'prachi@g.com', '123', NULL, NULL, NULL, 1, NULL, '2018-12-27 17:27:52'),
(65, 'prthmsh', 'singh', 'prathameshsingh@gmail.com', '12345678', NULL, NULL, NULL, 1, NULL, '2018-12-27 17:38:02'),
(66, 'vivek', 'singh', 'v@g.com', '123', NULL, NULL, NULL, 1, NULL, '2018-12-27 17:41:53'),
(67, 'ankur', 'singh', 'ankur@g.com', '123', NULL, NULL, NULL, 1, NULL, '2018-12-30 10:34:48'),
(68, 'rahul', 'gupta', 'r@f.com', '123', NULL, NULL, NULL, 1, NULL, '2019-01-14 15:20:24'),
(69, 'rahul', 'gupta', 'r@f.com', '123', NULL, NULL, NULL, 1, NULL, '2019-01-14 15:20:24'),
(70, 'Bhanu', 'Kashyap', 'abc@gmail.com', '123', NULL, NULL, NULL, 1, NULL, '2019-01-14 15:43:02'),
(71, 'ankur', 'sharma', 'ankur@yahoo.com', '123', NULL, NULL, NULL, 1, NULL, '2019-01-21 15:30:21'),
(72, 'Manoj', 'Dubey', 'm@g.com', '123', NULL, NULL, NULL, 1, NULL, '2019-01-29 00:14:35'),
(73, 'tanuja', 'dubey', 't@g.com', '123', NULL, NULL, NULL, 1, NULL, '2019-01-29 00:18:22'),
(74, 'Rishabh', 'singh', 'rish@g.com', '123', NULL, NULL, NULL, 1, NULL, '2019-01-29 00:49:49'),
(75, 'raju', 'sharma', 'raju@g.com', '123', NULL, NULL, NULL, 1, NULL, '2019-01-29 01:14:35'),
(76, 'yat', 'sharma', 'yat@y.com', '123', NULL, NULL, NULL, 1, NULL, '2019-01-29 10:21:03'),
(77, 'ram', 'sharma', 'r@y.com', '123', NULL, NULL, NULL, 1, NULL, '2019-01-29 10:41:02'),
(78, 'kiran', 'sharma', 'k@g.com', '123', NULL, NULL, NULL, 1, NULL, '2019-01-29 12:48:38'),
(79, 'Kundan', 'Ithape', 'kundanithape96@gmail.com', '123456', NULL, NULL, NULL, 1, NULL, '2019-01-29 13:22:09'),
(80, 'manali', 'chidrawar', 'manali@gmail.com', '1234', NULL, NULL, NULL, 0, NULL, '2019-01-29 14:09:30'),
(81, 'kiran', 'jadhav', 'kjadhav248@gmail.com', '12345', NULL, NULL, NULL, 1, NULL, '2019-01-29 16:09:04'),
(82, 'abhishek', 'chaubey', 'a@g.com', '123', NULL, NULL, NULL, 1, NULL, '2019-01-30 04:41:04'),
(83, 'pritesh', 'choudhary', 'p@g.com', '123', NULL, NULL, NULL, 1, NULL, '2019-01-30 08:43:32');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Account`
--
ALTER TABLE `Account`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userId` (`userId`);

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Account`
--
ALTER TABLE `Account`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=84;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `Account`
--
ALTER TABLE `Account`
  ADD CONSTRAINT `Account_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `users` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
