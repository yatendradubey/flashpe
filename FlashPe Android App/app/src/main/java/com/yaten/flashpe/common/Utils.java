package com.yaten.flashpe.common;

import android.util.Log;

public class Utils {
    private static String TAG = "Utils";

    public static String createUrl(String api) {
        String url = Constants.SERVER_URL + api;
        Log.d(TAG, "URL: " + url);
        return url;
    }

    public static String createImageUrl(String image) {
        String url = Constants.SERVER_URL + "/" + image;
        Log.d(TAG, "URL: " + url);
        return url;
    }
}
