package com.yaten.flashpe.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.yaten.flashpe.R;

public class BaseActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    protected void showDefault(View view, String message) {
        Snackbar snackbar = Snackbar.make(view,message, Snackbar.LENGTH_SHORT)
                .setAction("Action", null);
        View sbView = snackbar.getView();
        int colorSnackbarDefault = ContextCompat.getColor(this,R.color.colorSnackbarDefault);
        sbView.setBackgroundColor(colorSnackbarDefault);
        snackbar.show();
    }
    protected void showWarning(View view, String message) {

        Snackbar snackbar = Snackbar.make(view,message, Snackbar.LENGTH_SHORT)
                .setAction("Action", null);
        View sbView = snackbar.getView();
        int colorSnackbarWarning = ContextCompat.getColor(this,R.color.colorSnackbarWarning);
        sbView.setBackgroundColor(colorSnackbarWarning);
        snackbar.show();
    }

    protected void showSuccess(View view, String message) {
        Snackbar snackbar = Snackbar.make(view,message, Snackbar.LENGTH_SHORT)
                .setAction("Action", null);
        View sbView = snackbar.getView();
        int colorSnackbarSuccess = ContextCompat.getColor(this,R.color.colorSnackbarSuccess);
        sbView.setBackgroundColor(colorSnackbarSuccess);
        snackbar.show();
    }
    protected void showError(View view, String message) {
        Snackbar snackbar = Snackbar.make(view,message, Snackbar.LENGTH_SHORT)
                .setAction("Retry", null);
        View sbView = snackbar.getView();
        int colorSnackbarError = ContextCompat.getColor(this,R.color.colorSnackbarError);
        sbView.setBackgroundColor(colorSnackbarError);
        snackbar.show();
    }
    protected void showInfo(View view, String message) {
        Snackbar snackbar = Snackbar.make(view,message, Snackbar.LENGTH_SHORT)
                .setAction("Action", null);
        View sbView = snackbar.getView();
        int colorSnackbarInfo = ContextCompat.getColor(this,R.color.colorSnackbarInfo);
        sbView.setBackgroundColor(colorSnackbarInfo);
        snackbar.show();
    }

}
