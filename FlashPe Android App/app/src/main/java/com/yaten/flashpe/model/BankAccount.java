package com.yaten.flashpe.model;

public class BankAccount {
    private int id;
    private String accountHolderName;
    private int accountNumber;
    private int phone;
    private String upi;
    private double amount;
    private String bankName;
    private String branch;
    private double walletMoney;

    public double getWalletMoney() {
        return walletMoney;
    }

    public void setWalletMoney(double walletMoney) {
        this.walletMoney = walletMoney;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAccountHolderName() {
        return accountHolderName;
    }

    public void setAccountHolderName(String accountHolderName) {
        this.accountHolderName = accountHolderName;
    }

    public int getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(int accountNumber) {
        this.accountNumber = accountNumber;
    }

    public int getPhone() {
        return phone;
    }

    public void setPhone(int phone) {
        this.phone = phone;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getUpi() {
        return upi;
    }

    public void setUpi(String upi) {
        this.upi = upi;
    }

    @Override
    public String toString() {
        return "BankAccount{" +
                "id=" + id +
                ", accountHolderName='" + accountHolderName + '\'' +
                ", accountNumber=" + accountNumber +
                ", phone=" + phone +
                ", upi='" + upi + '\'' +
                ", amount=" + amount +
                ", bankName='" + bankName + '\'' +
                ", branch='" + branch + '\'' +
                ", walletMoney=" + walletMoney +
                '}';
    }
}
