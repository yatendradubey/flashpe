package com.yaten.flashpe.activity;

import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.yaten.flashpe.R;
import com.yaten.flashpe.adapter.BalanceAdapter;
import com.yaten.flashpe.common.Constants;
import com.yaten.flashpe.common.Utils;
import com.yaten.flashpe.model.BalanceItem;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CheckBalanceActivity extends ToolbarActivity {

    ArrayList<BalanceItem> balanceItems = new ArrayList<>();
    BalanceAdapter adapter;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.layoutEmpty)
    LinearLayout layoutEmpty;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_balance);
        ButterKnife.bind(this);

        adapter = new BalanceAdapter(this, balanceItems);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 1));
        recyclerView.setAdapter(adapter);

        displayToolbar();
    }

    @Override
    protected void onStart() {
        super.onStart();
        loadBalanceItems();
    }

    private void loadBalanceItems() {
        int userId = PreferenceManager.getDefaultSharedPreferences(this).getInt("userId", 0);

        Ion.with(this)
                .load("GET", Utils.createUrl(Constants.API_CHECKBALANCE + "/" + userId))
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        balanceItems.clear();

                        JsonArray data = result.get("data").getAsJsonArray();
                        for (int index = 0; index < data.size(); index++) {
                            JsonObject object = data.get(index).getAsJsonObject();

                            BalanceItem item = new BalanceItem();
                            item.setId(object.get("userId").getAsInt());
                            item.setAmount(object.get("Amount").getAsDouble());

                            balanceItems.add(item);
                        }

                        adapter.notifyDataSetChanged();

                        if (balanceItems.size() == 0) {
                            layoutEmpty.setVisibility(View.VISIBLE);
                            recyclerView.setVisibility(View.GONE);
                        } else {
                            layoutEmpty.setVisibility(View.GONE);
                            recyclerView.setVisibility(View.VISIBLE);
                        }

                    }
                });
    }
}
