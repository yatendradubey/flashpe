package com.yaten.flashpe.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.yaten.flashpe.R;
import com.yaten.flashpe.model.BankAccount;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BankAccountAdapter extends RecyclerView.Adapter<BankAccountAdapter.ViewHolder> {

    private final Context context;
    private final ArrayList<BankAccount> bankAccounts;
    @BindView(R.id.textUPIPin)
    TextView textUPIPin;
    @BindView(R.id.textBankName)
    TextView textBankName;
    @BindView(R.id.textBranchName)
    TextView textBranchName;

    public BankAccountAdapter(Context context, ArrayList<BankAccount> bankAccounts) {
        this.context = context;
        this.bankAccounts = bankAccounts;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.recyclerview_item_bank_accounts, null);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        final BankAccount bankAccount = bankAccounts.get(i);
        viewHolder.textBankAccountHolderName.setText(bankAccount.getAccountHolderName());
        viewHolder.textUPIPin.setText(bankAccount.getUpi());
        viewHolder.textBankName.setText(bankAccount.getBankName());
        viewHolder.textBranchName.setText(bankAccount.getBranch());
    }

    @Override
    public int getItemCount() {
        return bankAccounts.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.textBankAccountHolderName)
        TextView textBankAccountHolderName;
        @BindView(R.id.textUPIPin)
        TextView textUPIPin;
        @BindView(R.id.textBankName)
        TextView textBankName;
        @BindView(R.id.textBranchName)
        TextView textBranchName;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
