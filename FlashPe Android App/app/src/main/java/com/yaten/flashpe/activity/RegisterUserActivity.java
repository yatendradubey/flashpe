package com.yaten.flashpe.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.view.View;

import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.yaten.flashpe.R;
import com.yaten.flashpe.common.Constants;
import com.yaten.flashpe.common.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RegisterUserActivity extends BaseActivity {

    @BindView(R.id.editFirstName)
    TextInputEditText editFirstName;
    @BindView(R.id.editLastName)
    TextInputEditText editLastName;
    @BindView(R.id.editEmail)
    TextInputEditText editEmail;
    @BindView(R.id.editPassword)
    TextInputEditText editPassword;
    @BindView(R.id.editConfirmPassword)
    TextInputEditText editConfirmPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_user);
        ButterKnife.bind(this);

    }

    public void goToSigninActivity(View view) {
        Intent intent = new Intent(RegisterUserActivity.this, LoginActivity.class);
        startActivity(intent);
    }


    public void signup(final View view) {
        if (editFirstName.getText().toString().isEmpty()) {
            showWarning(view, "First Name is mandatory");
        } else if (editLastName.getText().toString().isEmpty()) {
            showWarning(view, "Last Name is mandatory");
        } else if (editEmail.getText().toString().isEmpty()) {
            showWarning(view, "Email is mandatory");
        } else if (editPassword.getText().toString().isEmpty()) {
            showWarning(view, "Password is mandatory");
        } else if (editConfirmPassword.getText().toString().isEmpty())
            showWarning(view, "Confirm password is mandatory");
        else {
            String email = editEmail.getText().toString().trim();
            String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
            String password = editPassword.getText().toString().trim();
            String confirmPassword = editConfirmPassword.getText().toString().trim();

            if (!email.matches(emailPattern)) {
                showError(view, "Invalid Email Address");
            } else if (!confirmPassword.matches(password)) {
                showError(view, "Passwords do not match");
            } else {
                JsonObject body = new JsonObject();
                body.addProperty("firstName", editFirstName.getText().toString());
                body.addProperty("lastName", editLastName.getText().toString());
                body.addProperty("email", editEmail.getText().toString());
                body.addProperty("password", editPassword.getText().toString());

                Ion.with(this)
                        .load(Utils.createUrl(Constants.API_USER + "/register"))
                        .setJsonObjectBody(body)
                        .asJsonObject()
                        .setCallback(new FutureCallback<JsonObject>() {
                            @Override
                            public void onCompleted(Exception e, JsonObject result) {
                                // do stuff with the result or error
                            finish();
                            }
                        });
            }
        }

    }
}
