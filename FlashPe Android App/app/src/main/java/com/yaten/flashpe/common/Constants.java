package com.yaten.flashpe.common;

public class Constants {

    // server url
    public static final String SERVER_URL = "http://192.168.43.88:3000";

    // result
    public static final String SUCCESS = "success";
    public static final String ERROR = "error";

    // apis

    public static final String API_USER = "/user";
    public static final String API_UPI = "/upi";
    public static final String API_SendMoney = "/user/sendMoney";
    public static final String API_BANKACCOUNT = "/bankAccount";
    public static final String API_ADDBANKACCOUNT = "/user/addAccount";
    public static final String API_CREATEUPIPIN = "/user/createUpiPin";
    public static final String API_CHECKUPIPIN = "/user/checkUpiPin";
    public static final String API_CHECKUPIID = "/user/checkUpiId";
    public static final String API_CHECKBALANCE = "/user/checkBalance";
    public static final String API_ADDCARD= "/user/addCard";
    public static final String API_GETCARD= "/user/getCard";


    public static final String API_CHECKCVV = "/user/checkCVV";
    public static final String API_ADDMONEYTOWALLET = "/user/addMoneyToWallet";
    public static final String API_SENDWALLETMONEY = "/user/sendWalletMoney";
    public static final String API_SendMoneyTransaction = "/user/sendMoneyTransaction";
}
