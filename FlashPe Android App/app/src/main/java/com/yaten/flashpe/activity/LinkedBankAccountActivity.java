package com.yaten.flashpe.activity;

import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.yaten.flashpe.R;
import com.yaten.flashpe.adapter.BankAccountAdapter;
import com.yaten.flashpe.common.Constants;
import com.yaten.flashpe.common.Utils;
import com.yaten.flashpe.model.BankAccount;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LinkedBankAccountActivity extends ToolbarActivity {

    @BindView(R.id.rc)
    RecyclerView rc;

    ArrayList<BankAccount> bankAccounts = new ArrayList<>();
    @BindView(R.id.layoutEmpty)
    LinearLayout layoutEmpty;
    private BankAccountAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bank_account);
        ButterKnife.bind(this);

        displayToolbar();

        adapter = new BankAccountAdapter(this, bankAccounts);
        rc.setLayoutManager(new GridLayoutManager(this, 1));
        rc.setAdapter(adapter);


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LinkedBankAccountActivity.this, AddBankAccountActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        loadBankAccounts();
    }

    private void loadBankAccounts() {
        bankAccounts.clear();
        int userId = PreferenceManager.getDefaultSharedPreferences(this).getInt("userId", 0);
        Ion.with(this)
                .load("GET", Utils.createUrl(Constants.API_BANKACCOUNT + "/" + userId))
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        // bankAccounts.clear();

                        if (result.get("status").getAsString().equals(Constants.SUCCESS)) {
                            JsonArray data = result.get("data").getAsJsonArray();
                            for (int index = 0; index < data.size(); index++) {
                                JsonObject object = data.get(index).getAsJsonObject();

                                BankAccount bankAccount = new BankAccount();
                                bankAccount.setAccountHolderName(object.get("accountHolderName").getAsString());
                                bankAccount.setBankName(object.get("BankName").getAsString());
                                bankAccount.setBranch(object.get("Branch").getAsString());
                                if(!object.get("upi").isJsonNull()){
                                    bankAccount.setUpi(object.get("upi").getAsString());
                                }
                                bankAccounts.add(bankAccount);
                            }
                            adapter.notifyDataSetChanged();

                            if (bankAccounts.size() == 0) {
                                layoutEmpty.setVisibility(View.VISIBLE);
                                rc.setVisibility(View.GONE);
                            } else {
                                layoutEmpty.setVisibility(View.GONE);
                                rc.setVisibility(View.VISIBLE);
                            }
                        }
                    }

                });
    }

}

