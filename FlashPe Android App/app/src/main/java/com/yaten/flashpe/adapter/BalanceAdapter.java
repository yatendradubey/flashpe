package com.yaten.flashpe.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.yaten.flashpe.R;
import com.yaten.flashpe.model.BalanceItem;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BalanceAdapter extends RecyclerView.Adapter<BalanceAdapter.ViewHolder> {

    private final Context context;
    private final ArrayList<BalanceItem> balanceItems;

    public BalanceAdapter(Context context, ArrayList<BalanceItem> balanceItems) {
        this.context = context;
        this.balanceItems = balanceItems;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(this.context).inflate(R.layout.recyclerview_item_bank_balance, null);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        final BalanceItem item = balanceItems.get(i);
        viewHolder.textAccountBalance.setText("₹ " + item.getAmount());
    }


    @Override
    public int getItemCount() {
        return balanceItems.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.textAccountBalance)
        TextView textAccountBalance;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
