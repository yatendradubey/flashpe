package com.yaten.flashpe.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.card.MaterialCardView;
import android.support.design.widget.TextInputEditText;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.yaten.flashpe.R;
import com.yaten.flashpe.adapter.WalletAdapter;
import com.yaten.flashpe.common.Constants;
import com.yaten.flashpe.common.Utils;
import com.yaten.flashpe.model.BankAccount;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WalletHomeActivity extends ToolbarActivity {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    ArrayList<BankAccount> bankAccounts = new ArrayList<>();
    WalletAdapter adapter;
    @BindView(R.id.editWalletTransactionAmount)
    TextInputEditText editWalletTransactionAmount;
    @BindView(R.id.buttonContacts)
    ImageView buttonContacts;
    @BindView(R.id.hiddenLayout)
    MaterialCardView hiddenLayout;
    @BindView(R.id.buttonProceed)
    Button buttonProceed;
    @BindView(R.id.editPhoneNumber)
    TextInputEditText editPhoneNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet_home);
        ButterKnife.bind(this);
        displayToolbar();

        adapter = new WalletAdapter(this, bankAccounts);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 1));
        recyclerView.setAdapter(adapter);

    }

    @Override
    protected void onStart() {
        super.onStart();
        loadBalanceItems();
    }

    private void loadBalanceItems() {
        int userId = PreferenceManager.getDefaultSharedPreferences(this).getInt("userId", 0);

        Ion.with(this)
                .load("GET", Utils.createUrl(Constants.API_CHECKBALANCE + "/" + userId))
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        bankAccounts.clear();

                        JsonArray data = result.get("data").getAsJsonArray();
                        for (int index = 0; index < data.size(); index++) {
                            JsonObject object = data.get(index).getAsJsonObject();

                            BankAccount item = new BankAccount();
                            item.setId(object.get("userId").getAsInt());
                            item.setWalletMoney(object.get("walletMoney").getAsDouble());

                            bankAccounts.add(item);
                        }

                        adapter.notifyDataSetChanged();

                    }
                });
    }

    public void AddWalletMoney(View view) {
        String WTAmount = editWalletTransactionAmount.getText().toString();

        if (WTAmount.isEmpty()) {
            showWarning(view, "Enter the amount");
        } else {

            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(WalletHomeActivity.this);
            preferences
                    .edit()
                    .putString("WTAmount", WTAmount)
                    .putInt("txnType", 2)
                    .apply();
            Intent intent = new Intent(this, AddMoneyToWalletActivity.class);
            startActivity(intent);

        }
    }

    public void SendWalletMoney(View view) {
        String walletSendAmount = editWalletTransactionAmount.getText().toString();
        // Log.e("WalletHomeActivity: ","txnType: "+txnType);
        if (walletSendAmount.isEmpty()) {
            showWarning(view, "Enter the amount");
        } else {
            hiddenLayout.setVisibility(View.VISIBLE);
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(WalletHomeActivity.this);
            preferences
                    .edit()
                    .putString("walletSendAmount", walletSendAmount)
                    .putInt("txnType", 3)
                    .apply();

        }
    }

    public void Proceed(View view) {
        hideKeyboardPanel();
        String receiverPhoneNumber = editPhoneNumber.getText().toString();
        if (receiverPhoneNumber.isEmpty()){
            showWarning(view,"Enter the phone number");
        }else{
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(WalletHomeActivity.this);
            preferences
                    .edit()
                    .putString("receiverPhoneNumber", receiverPhoneNumber)
                    .apply();
            Intent intent = new Intent(this, AddMoneyToWalletActivity.class);
            startActivity(intent);
        }

    }

    private void hideKeyboardPanel() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(buttonProceed.getWindowToken(), 0);

    }
}
