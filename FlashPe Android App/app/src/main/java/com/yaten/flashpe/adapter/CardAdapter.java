package com.yaten.flashpe.adapter;

import android.content.Context;
import android.content.Intent;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.yaten.flashpe.R;
import com.yaten.flashpe.activity.AddMoneyToWalletActivity;
import com.yaten.flashpe.activity.BaseActivity;
import com.yaten.flashpe.activity.SuccessfulTransactionActivity;
import com.yaten.flashpe.activity.WalletHomeActivity;
import com.yaten.flashpe.common.Constants;
import com.yaten.flashpe.common.Utils;
import com.yaten.flashpe.model.CardItem;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CardAdapter extends RecyclerView.Adapter<CardAdapter.ViewHolder> {

    @BindView(R.id.editRecyclerViewCvv)
    EditText editRecyclerViewCvv;
    public interface EventListener {
        void onSelect(CardItem cardItem);
    }
    private final Context context;
    private final ArrayList<CardItem> cardItems;
    private final EventListener listener;

    public CardAdapter(Context context, ArrayList<CardItem> cardItems) {
        this.context = context;
        this.cardItems = cardItems;
        this.listener = (EventListener) context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(this.context).inflate(R.layout.recyclerview_item_card, null);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, int i) {
        final CardItem cardItem = cardItems.get(i);

        Log.e("cardNumber", "Value: " + cardItem.getCardNumber());
        viewHolder.textBankName.setText(cardItem.getBankName());
        viewHolder.textDebitCardNumber.setText(Long.toString(cardItem.getCardNumber()));
        //viewHolder.editRecyclerViewCvv.setText(cardItem.getCvv());
       // final String test = viewHolder.editRecyclerViewCvv.getText().toString();

        viewHolder.buttonProceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onSelect(cardItem);
                String cvv = viewHolder.editRecyclerViewCvv.getText().toString();
                //String cardNumber = viewHolder.textDebitCardNumber.getText().toString();
                if(cvv.isEmpty()){
                    Toast.makeText(context, "Cvv is mandatory", Toast.LENGTH_SHORT).show();
                }else {
                    final int userId = PreferenceManager.getDefaultSharedPreferences(context).getInt("userId", 0);
                    int txnType = PreferenceManager.getDefaultSharedPreferences(context).getInt("txnType",0);
                    if(txnType == 2){
                        Log.e("CardAdapter: ","txnType: "+txnType);
                        JsonObject body = new JsonObject();
                        body.addProperty("cvv", cvv);
                        body.addProperty("userId",userId);
                        body.addProperty("cardNumber",cardItem.getCardNumber());
                        Ion.with(context)
                                .load("GET", Utils.createUrl(Constants.API_CHECKCVV + "/" + userId))
                                .setJsonObjectBody(body)
                                .asJsonObject()
                                .setCallback(new FutureCallback<JsonObject>() {
                                    @Override
                                    public void onCompleted(Exception e, JsonObject result) {
                                        if(result.get("status").getAsString().equals(Constants.ERROR)){
                                            Toast.makeText(context, "Incorrect cvv", Toast.LENGTH_SHORT).show();
                                        }else{
                                            int userId = PreferenceManager.getDefaultSharedPreferences(context).getInt("userId", 0);
                                            String WTAmount = (String) PreferenceManager.getDefaultSharedPreferences(context).getString("WTAmount", String.valueOf(0));
                                            Log.e("WTAmount","WTAmount: "+WTAmount);
                                            JsonObject body1 = new JsonObject();
                                            body1.addProperty("WTAmount",WTAmount);

                                            Ion.with(context)
                                                    .load("PUT",Utils.createUrl(Constants.API_ADDMONEYTOWALLET + "/" +userId))
                                                    .setJsonObjectBody(body1)
                                                    .asJsonObject()
                                                    .setCallback(new FutureCallback<JsonObject>() {
                                                        @Override
                                                        public void onCompleted(Exception e, JsonObject result) {
                                                            // do stuff with the result or error
                                                            if (result.get("status").getAsString().equals(Constants.SUCCESS)) {
                                                                Intent intent = new Intent(context, SuccessfulTransactionActivity.class);
                                                                context.startActivity(intent);
                                                                ((AddMoneyToWalletActivity)context).finish();
                                                            }else{
                                                                Toast.makeText(context, "Transaction Unsuccessful", Toast.LENGTH_SHORT).show();
                                                            }
                                                            //finish();
                                                        }
                                                    });

                                        }
                                    }
                                });
                    }else{

                        Log.e("CardAdapter: ","txnType: "+txnType);
                       // Toast.makeText(context, "HI", Toast.LENGTH_SHORT).show();
                        JsonObject body1 = new JsonObject();
                        body1.addProperty("cvv", cvv);
                        body1.addProperty("userId",userId);
                        body1.addProperty("cardNumber",cardItem.getCardNumber());
                        Ion.with(context)
                                .load("GET",Utils.createUrl(Constants.API_CHECKCVV + "/" + userId))
                                .setJsonObjectBody(body1)
                                .asJsonObject()
                                .setCallback(new FutureCallback<JsonObject>() {
                                    @Override
                                    public void onCompleted(Exception e, JsonObject result) {
                                        if(result.get("status").getAsString().equals(Constants.ERROR)){

                                            Toast.makeText(context, "Incorrect cvv", Toast.LENGTH_SHORT).show();
                                        }else{
                                            String receiverPhoneNumber = PreferenceManager.getDefaultSharedPreferences(context).getString("receiverPhoneNumber", null);
                                            String WTAmount = (String) PreferenceManager.getDefaultSharedPreferences(context).getString("WTAmount", String.valueOf(0));
                                            Log.e("CardAdapter: ","receiverPhoneNumber: "+receiverPhoneNumber);
                                            Log.e("CardAdapter: ","WTAmount: "+WTAmount);
                                            JsonObject body = new JsonObject();
                                            body.addProperty("userId",userId);
                                            body.addProperty("receiverPhoneNumber",receiverPhoneNumber);
                                            body.addProperty("sendAmount",WTAmount);

                                            Ion.with(context)
                                                    .load("PUT",Utils.createUrl(Constants.API_SENDWALLETMONEY + "/"+userId))
                                                    .setJsonObjectBody(body)
                                                    .asJsonObject()
                                                    .setCallback(new FutureCallback<JsonObject>() {
                                                        @Override
                                                        public void onCompleted(Exception e, JsonObject result) {
                                                            if(result.get("status").getAsString().equals("success")){
                                                                Intent intent = new Intent(context, SuccessfulTransactionActivity.class);
                                                                context.startActivity(intent);
                                                                ((AddMoneyToWalletActivity)context).finish();
                                                            }else {
                                                                Toast.makeText(context, "Transaction Unsuccessful", Toast.LENGTH_SHORT).show();
                                                            }
                                                        }
                                                    });
                                        }
                                    }
                                });
                    }
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return cardItems.size();
    }


    static class ViewHolder extends RecyclerView.ViewHolder {
        private final View view;
        @BindView(R.id.textBankName)
        TextView textBankName;
        @BindView(R.id.textDebitCardNumber)
        TextView textDebitCardNumber;
        @BindView(R.id.editRecyclerViewCvv)
        EditText editRecyclerViewCvv;
        @BindView(R.id.buttonProceed)
        TextView buttonProceed;

        ViewHolder(View view) {
            super(view);
            this.view = view;
            ButterKnife.bind(this, view);
        }
    }
}
