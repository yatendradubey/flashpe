package com.yaten.flashpe.activity;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.TextInputEditText;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;

import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.shashank.sony.fancydialoglib.Animation;
import com.shashank.sony.fancydialoglib.FancyAlertDialog;
import com.shashank.sony.fancydialoglib.FancyAlertDialogListener;
import com.shashank.sony.fancydialoglib.Icon;
import com.yaten.flashpe.R;
import com.yaten.flashpe.common.Constants;
import com.yaten.flashpe.common.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AddBankAccountActivity extends ToolbarActivity {

    @BindView(R.id.editPhone)
    TextInputEditText editPhone;
    @BindView(R.id.buttonAddAccount)
    Button buttonAddAccount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_bank_account);
        ButterKnife.bind(this);

        displayToolbar();
    }

    public void addAccount(final View view) {
        hideKeyboardPanel();
        if(editPhone.getText().toString().isEmpty()){
            showWarning(view,"Enter the phone number");
        }else {
            int userId = PreferenceManager.getDefaultSharedPreferences(this).getInt("userId", 0);
            JsonObject body = new JsonObject();
            body.addProperty("phone", editPhone.getText().toString().trim());

            Ion.with(this)
                    .load("PUT", Utils.createUrl(Constants.API_ADDBANKACCOUNT + "/"+userId))
                    .setJsonObjectBody(body)
                    .asJsonObject()
                    .setCallback(new FutureCallback<JsonObject>() {

                        @Override
                        public void onCompleted(Exception e, JsonObject result) {
                            if (result.get("status").getAsString().equals(Constants.ERROR)) {
                                showError(view, result.get("message").getAsString());
//                                Log.e("test1","hello");
//                                showWarning(view,"error");
                            } else {
                             //   Log.e("test2","hello");
                                new FancyAlertDialog.Builder(AddBankAccountActivity.this)
                                        .setTitle("Account Added Successfully")
                                        .setBackgroundColor(Color.parseColor("#4CAF50"))  //Don't pass R.color.colorvalue
                                        .setNegativeBtnText("Cancel")
                                        .setPositiveBtnBackground(Color.parseColor("#6193C7"))  //Don't pass R.color.colorvalue
                                        .setPositiveBtnText("ok")
                                        .setNegativeBtnBackground(Color.parseColor("#403C3C"))  //Don't pass R.color.colorvalue
                                        .setAnimation(Animation.POP)
                                        .isCancellable(true)
                                        .setIcon(R.drawable.ic_correct,Icon.Visible)
                                        .OnPositiveClicked(new FancyAlertDialogListener() {
                                            @Override
                                            public void OnClick() {
                                                finish();
                                            }
                                        })
                                        .OnNegativeClicked(new FancyAlertDialogListener() {
                                            @Override
                                            public void OnClick() {
                                            }
                                        })
                                        .build();
                            }
                        }
                    });

        }
    }

    private void hideKeyboardPanel() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(buttonAddAccount.getWindowToken(), 0);
    }
}
