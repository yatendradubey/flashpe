package com.yaten.flashpe.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.TextInputEditText;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.yaten.flashpe.R;
import com.yaten.flashpe.common.Constants;
import com.yaten.flashpe.common.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LoginActivity extends BaseActivity {

    @BindView(R.id.editEmail)
    TextInputEditText editEmail;
    @BindView(R.id.editPassword)
    TextInputEditText editPassword;
    @BindView(R.id.checkboxRememberMe)
    CheckBox checkboxRememberMe;
    @BindView(R.id.textForgotPassword)
    TextView textForgotPassword;
    @BindView(R.id.buttonSignIn)
    Button buttonSignIn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

    }

    public void goToSignUpActivity(View view) {
        Intent intent = new Intent(LoginActivity.this, RegisterUserActivity.class);
        startActivity(intent);
    }

    public void signin(final View view) {
        hideKeyboardPanel();

        if (editEmail.getText().toString().isEmpty()) {
            showWarning(view, "Email is manadatory");
        } else if (editPassword.getText().toString().isEmpty()) {
            showWarning(view, "Password is manadatory");
        } else {
            JsonObject body = new JsonObject();
            body.addProperty("email", editEmail.getText().toString().trim());
            body.addProperty("password", editPassword.getText().toString().trim());

            Ion.with(this)
                    .load("POST", Utils.createUrl(Constants.API_USER + "/login"))
                    .setJsonObjectBody(body)
                    .asJsonObject()
                    .setCallback(new FutureCallback<JsonObject>() {

                        @Override
                        public void onCompleted(Exception e, JsonObject result) {
                            System.out.print(result);
                            if (result.get("status").getAsString().equals(Constants.ERROR)) {
                                showError(view, result.get("message").getAsString());
                                System.out.print(result);
                            } else {
                                showSuccess(view, "Successful login");

                                JsonObject data = result.get("data").getAsJsonObject();

                                String userName = data.get("firstName").getAsString() + " " + data.get("lastName").getAsString();

                                SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(LoginActivity.this);
                                preferences
                                        .edit()
                                        .putBoolean("loginStatus", true)
                                        .putInt("userId", data.get("id").getAsInt())
                                        .putString("userName", userName)
                                        .putString("userEmail", data.get("email").getAsString())
                                        .commit();

                                Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                                startActivity(intent);
                                finish();
                            }
                        }
                    });

        }
    }

    private void hideKeyboardPanel() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(buttonSignIn.getWindowToken(), 0);

    }
}
