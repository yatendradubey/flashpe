package com.yaten.flashpe.model;

public class BalanceItem {
  private int id;
  private Double amount;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "BalanceItem{" +
                "id=" + id +
                ", amount=" + amount +
                '}';
    }
}
