package com.yaten.flashpe.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.TextInputEditText;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;

import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.shashank.sony.fancydialoglib.Animation;
import com.shashank.sony.fancydialoglib.FancyAlertDialog;
import com.shashank.sony.fancydialoglib.FancyAlertDialogListener;
import com.shashank.sony.fancydialoglib.Icon;
import com.yaten.flashpe.R;
import com.yaten.flashpe.common.Constants;
import com.yaten.flashpe.common.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CreateUpiPinActivity extends ToolbarActivity {

    @BindView(R.id.buttonCreateUPI)
    Button buttonCreateUPI;
    @BindView(R.id.editUpiPin)
    TextInputEditText editUpiPin;
    @BindView(R.id.editConfirmUpiPin)
    TextInputEditText editConfirmUpiPin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_upi_pin);
        ButterKnife.bind(this);

        displayToolbar();
    }

    public void createUpiPin(final View view) {
        hideKeyboardPanel();
        if (editUpiPin.getText().toString().isEmpty()) {
            showWarning(view, "Enter the UPI Pin");
        } else if (editConfirmUpiPin.getText().toString().isEmpty()) {
            showWarning(view, "Enter the Confirm UPI Pin");
        } else {
            String upiPin = editUpiPin.getText().toString().trim();
            String confirmUpiPin = editConfirmUpiPin.getText().toString().trim();

            if(!confirmUpiPin.matches(upiPin)){
                showError(view,"Upi Pins do not match.");
            }
            else if(editUpiPin.length() != 4 || editConfirmUpiPin.length()!=4 ){
                showWarning(view,"UPI Pin must be of the four digits");
            } else{
                int userId = PreferenceManager.getDefaultSharedPreferences(this).getInt("userId", 0);
                JsonObject body = new JsonObject();
                body.addProperty("upiPin", editUpiPin.getText().toString().trim());

                Ion.with(this)
                        .load("PUT", Utils.createUrl(Constants.API_CREATEUPIPIN + "/"+userId))
                        .setJsonObjectBody(body)
                        .asJsonObject()
                        .setCallback(new FutureCallback<JsonObject>() {

                            @Override
                            public void onCompleted(Exception e, JsonObject result) {
                                if (result.get("status").getAsString().equals(Constants.ERROR)) {
                                    showError(view, result.get("message").getAsString());
                                } else {
                                    new FancyAlertDialog.Builder(CreateUpiPinActivity.this)
                                            .setTitle("UPI Pin Created Successfully")
                                            .setBackgroundColor(Color.parseColor("#4CAF50"))  //Don't pass R.color.colorvalue
                                            .setNegativeBtnText("Cancel")
                                            .setPositiveBtnBackground(Color.parseColor("#6193C7"))  //Don't pass R.color.colorvalue
                                            .setPositiveBtnText("ok")
                                            .setNegativeBtnBackground(Color.parseColor("#403C3C"))  //Don't pass R.color.colorvalue
                                            .setAnimation(Animation.POP)
                                            .isCancellable(true)
                                            .setIcon(R.drawable.ic_correct, Icon.Visible)
                                            .OnPositiveClicked(new FancyAlertDialogListener() {
                                                @Override
                                                public void OnClick() {
                                                    Intent intent = new Intent(CreateUpiPinActivity.this,HomeActivity.class);
                                                    startActivity(intent);
                                                }
                                            })
                                            .OnNegativeClicked(new FancyAlertDialogListener() {
                                                @Override
                                                public void OnClick() {
                                                }
                                            })
                                            .build();
                                }
                            }
                        });

            }
        }
    }
    private void hideKeyboardPanel() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(buttonCreateUPI.getWindowToken(), 0);

    }
}
