package com.yaten.flashpe.activity;

import android.content.Context;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.yaten.flashpe.R;
import com.yaten.flashpe.adapter.CardAdapter;
import com.yaten.flashpe.common.Constants;
import com.yaten.flashpe.common.Utils;
import com.yaten.flashpe.model.CardItem;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AddMoneyToWalletActivity extends ToolbarActivity implements CardAdapter.EventListener {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.editCardNumber)
    EditText editCardNumber;
    @BindView(R.id.editExpiryMonth)
    EditText editExpiryMonth;
    @BindView(R.id.editExpiryYear)
    EditText editExpiryYear;
    @BindView(R.id.editCvv)
    EditText editCvv;
    @BindView(R.id.buttonProceedSecurely)
    Button buttonProceedSecurely;
//    @BindView(R.id.editRecyclerViewCvv)
//    EditText editRecyclerViewCvv;

    ArrayList<CardItem> cardItems = new ArrayList<>();
    CardAdapter adapter;
    @BindView(R.id.textEmptyLayout)
    RelativeLayout textEmptyLayout;
    @BindView(R.id.cardView)
    CardView cardView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_money_to_wallet);
        ButterKnife.bind(this);
        displayToolbar();

        adapter = new CardAdapter(this, cardItems);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, true));
        recyclerView.setAdapter(adapter);

    }

    @Override
    protected void onStart() {
        super.onStart();
        loadUserDebitCards();
    }

    private void loadUserDebitCards() {
        cardItems.clear();
        int userId = PreferenceManager.getDefaultSharedPreferences(this).getInt("userId", 0);
        int txnType = PreferenceManager.getDefaultSharedPreferences(this).getInt("txnType",0);
        String receiverPhoneNumber = PreferenceManager.getDefaultSharedPreferences(this).getString("receiverPhoneNumber",null);
        Log.e("AddmoneyToWalletActivity: ","txnType: "+txnType);
        Log.e("AddmoneyToWalletActivity: ","receiverPhoneNumber: "+receiverPhoneNumber);
        Ion.with(this)
                .load("GET", Utils.createUrl(Constants.API_GETCARD + "/" + userId))
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        if (result.get("status").getAsString().equals(Constants.SUCCESS)) {
                            JsonArray data = result.get("data").getAsJsonArray();
                            for (int index = 0; index < data.size(); index++) {
                                JsonObject object = data.get(index).getAsJsonObject();

                                CardItem cardItem = new CardItem();
                                cardItem.setBankName(object.get("BankName").getAsString());
                                cardItem.setCardNumber(object.get("cardNumber").getAsLong());

                                cardItems.add(cardItem);
                            }
                            adapter.notifyDataSetChanged();
                            if (cardItems.size() == 0) {
                                textEmptyLayout.setVisibility(View.VISIBLE);
                                //textEmptyLayout.setVisibility(View.GONE);
                                recyclerView.setVisibility(View.GONE);
                            } else {
                                //textEmptyLayout.setVisibility(View.GONE);
                                textEmptyLayout.setVisibility(View.GONE);
                                recyclerView.setVisibility(View.VISIBLE);
                            }
                        }
                    }

                });
    }

    private void updateUserDebitCards() {
        cardItems.clear();
        int userId = PreferenceManager.getDefaultSharedPreferences(this).getInt("userId", 0);
        Ion.with(this)
                .load("GET", Utils.createUrl(Constants.API_GETCARD + "/" + userId))
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        if (result.get("status").getAsString().equals(Constants.SUCCESS)) {
                            JsonArray data = result.get("data").getAsJsonArray();
                            for (int index = 0; index < data.size(); index++) {
                                JsonObject object = data.get(index).getAsJsonObject();

                                CardItem cardItem = new CardItem();
                                cardItem.setBankName(object.get("BankName").getAsString());
                                cardItem.setCardNumber(object.get("cardNumber").getAsLong());
                                cardItems.add(cardItem);
                            }
                            if (cardItems.size() == 0) {
                                textEmptyLayout.setVisibility(View.VISIBLE);
                                //textEmptyLayout.setVisibility(View.GONE);
                                recyclerView.setVisibility(View.GONE);
                            } else {
                                textEmptyLayout.setVisibility(View.GONE);
                                //textEmptyLayout.setVisibility(View.VISIBLE);
                                recyclerView.setVisibility(View.VISIBLE);
                            }

                            if(cardItems.size()==1){
                                adapter.notifyDataSetChanged();
                            }else{
                                recyclerView.smoothScrollToPosition(recyclerView.getAdapter().getItemCount() - 1);
                                adapter.notifyDataSetChanged();
                            }

                        }
                    }

                });
    }

    public void addCard(final View view) {
        int userId = PreferenceManager.getDefaultSharedPreferences(this).getInt("userId", 0);
        JsonObject body = new JsonObject();
        body.addProperty("cardNumber", editCardNumber.getText().toString());
        body.addProperty("expiryMonth", editExpiryMonth.getText().toString());
        body.addProperty("expiryYear", editExpiryYear.getText().toString());
        body.addProperty("cvv", editCvv.getText().toString());

        Ion.with(this)
                .load("POST", Utils.createUrl(Constants.API_ADDCARD + "/" + userId))
                .setJsonObjectBody(body)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        // do stuff with the result or error
                        if (result.get("status").getAsString().equals(Constants.SUCCESS)) {
                            showSuccess(view, "Success");

                            updateUserDebitCards();
                            hideKeyboardPanel();
                        }
                        //finish();
                    }
                });
    }

    private void hideKeyboardPanel() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(buttonProceedSecurely.getWindowToken(), 0);

    }


    @Override
    public void onSelect(CardItem cardItem) {


    }
}
