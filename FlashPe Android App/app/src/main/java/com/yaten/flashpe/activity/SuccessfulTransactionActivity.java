package com.yaten.flashpe.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.yaten.flashpe.R;

public class SuccessfulTransactionActivity extends ToolbarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_successful_transaction);
        displayToolbar();
    }

    public void doAnotherTransaction(View view) {
        Intent intent = new Intent(this,WalletHomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
       // finish();
    }
}
