package com.yaten.flashpe.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.shashank.sony.fancydialoglib.Animation;
import com.shashank.sony.fancydialoglib.FancyAlertDialog;
import com.shashank.sony.fancydialoglib.FancyAlertDialogListener;
import com.shashank.sony.fancydialoglib.Icon;
import com.yaten.flashpe.R;
import com.yaten.flashpe.common.Constants;
import com.yaten.flashpe.common.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AddUpiIdActivity extends ToolbarActivity {

    @BindView(R.id.editUpiId)
    EditText editUpiId;
    @BindView(R.id.buttonAddUPI)
    Button buttonAddUPI;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_upi_id);
        ButterKnife.bind(this);

        displayToolbar();
    }

    public void addUpi(final View view) {
        if(editUpiId.getText().toString().isEmpty()){
            showWarning(view,"Enter the phone number");
        }else{
            int userId = PreferenceManager.getDefaultSharedPreferences(this).getInt("userId", 0);
            JsonObject body = new JsonObject();
            body.addProperty("upi", editUpiId.getText().toString().trim());

            Ion.with(this)
                    .load("PUT",Utils.createUrl(Constants.API_UPI + "/"+ userId))
                    .setJsonObjectBody(body)
                    .asJsonObject()
                    .setCallback(new FutureCallback<JsonObject>() {
                        @Override
                        public void onCompleted(Exception e, JsonObject result) {
                            System.out.print(result);
                            if (result.get("status").getAsString().equals(Constants.ERROR)) {
                                showError(view, result.get("message").getAsString());
                            } else {
                                new FancyAlertDialog.Builder(AddUpiIdActivity.this)
                                        .setTitle("UPI ID created Successfully")
                                        .setMessage("Create UPI PIN?")
                                        .setBackgroundColor(Color.parseColor("#4CAF50"))  //Don't pass R.color.colorvalue
                                        .setNegativeBtnText("Cancel")
                                        .setPositiveBtnBackground(Color.parseColor("#6193C7"))  //Don't pass R.color.colorvalue
                                        .setPositiveBtnText("ok")
                                        .setNegativeBtnBackground(Color.parseColor("#403C3C"))  //Don't pass R.color.colorvalue
                                        .setAnimation(Animation.POP)
                                        .isCancellable(true)
                                        .setIcon(R.drawable.ic_correct, Icon.Visible)
                                        .OnPositiveClicked(new FancyAlertDialogListener() {
                                            @Override
                                            public void OnClick() {
                                                Intent intent = new Intent(AddUpiIdActivity.this,CreateUpiPinActivity.class);
                                                startActivity(intent);
                                                //finish();
                                            }
                                        })
                                        .OnNegativeClicked(new FancyAlertDialogListener() {
                                            @Override
                                            public void OnClick() {
                                            }
                                        })
                                        .build();
                            }
                        }
                    });
        }
    }
}
