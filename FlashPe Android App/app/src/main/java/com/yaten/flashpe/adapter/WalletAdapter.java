package com.yaten.flashpe.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.yaten.flashpe.R;
import com.yaten.flashpe.model.BalanceItem;
import com.yaten.flashpe.model.BankAccount;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WalletAdapter extends RecyclerView.Adapter<WalletAdapter.ViewHolder> {
    private final Context context;
    private final ArrayList<BankAccount> bankAccounts;

    public WalletAdapter(Context context, ArrayList<BankAccount> bankAccounts) {
        this.context = context;
        this.bankAccounts = bankAccounts;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(this.context).inflate(R.layout.recyclerview_item_wallet,null);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        final BankAccount item = bankAccounts.get(i);
        viewHolder.textWalletMoney.setText("₹ " + item.getWalletMoney());
    }

    @Override
    public int getItemCount() {
        return bankAccounts.size();
    }

    static
    class ViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.textWalletMoney)
        TextView textWalletMoney;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
