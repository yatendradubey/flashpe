package com.yaten.flashpe.activity;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.TextInputEditText;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;

import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.shashank.sony.fancydialoglib.Animation;
import com.shashank.sony.fancydialoglib.FancyAlertDialog;
import com.shashank.sony.fancydialoglib.FancyAlertDialogListener;
import com.shashank.sony.fancydialoglib.Icon;
import com.yaten.flashpe.R;
import com.yaten.flashpe.common.Constants;
import com.yaten.flashpe.common.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SendMoneyActivity extends ToolbarActivity {


    @BindView(R.id.buttonSendMoney)
    Button buttonSendMoney;
    @BindView(R.id.editSendMoneyAmount)
    TextInputEditText editSendMoneyAmount;
    @BindView(R.id.editUpiPin)
    TextInputEditText editUpiPin;
    @BindView(R.id.editReceiverUpi)
    TextInputEditText editReceiverUpi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_money);
        ButterKnife.bind(this);

        displayToolbar();

    }


    public void sendMoney(final View view) {
        hideKeyboardPanel();
        if (editReceiverUpi.getText().toString().isEmpty()) {
            showWarning(view, "Enter the receiver upi");
        } else if (editSendMoneyAmount.getText().toString().isEmpty()) {
            showWarning(view, "Enter the amount");
        } else if (editUpiPin.getText().toString().isEmpty()) {
            showWarning(view, "Enter the UPI PIN");
        } else {
            int senderId = PreferenceManager.getDefaultSharedPreferences(this).getInt("userId", 0);
            checkUpiId(senderId, editReceiverUpi, view);
        }
    }

    private void checkUpiId(final int senderId, final TextInputEditText editReceiverUpi, final View view) {
        JsonObject body1 = new JsonObject();
        body1.addProperty("upiId", editReceiverUpi.getText().toString().trim());
        Ion.with(this)
                .load("GET", Utils.createUrl(Constants.API_CHECKUPIID + "/" + senderId))
                .setJsonObjectBody(body1)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        if (result.get("status").getAsString().equals(Constants.ERROR)) {
                            showError(view, result.get("message").getAsString());
                        } else {
                            JsonObject body1 = new JsonObject();
                            body1.addProperty("upiPin", editUpiPin.getText().toString().trim());
                            Ion.with(SendMoneyActivity.this)
                                    .load("POST", Utils.createUrl(Constants.API_CHECKUPIPIN + "/" + senderId))
                                    .setJsonObjectBody(body1)
                                    .asJsonObject()
                                    .setCallback(new FutureCallback<JsonObject>() {
                                        @Override
                                        public void onCompleted(Exception e, JsonObject result) {
                                            if (result.get("status").getAsString().equals(Constants.ERROR)) {
                                                showError(view, result.get("message").getAsString());
                                            } else {
                                                // showSuccess(view, "UPI pin correct Successfully");

                                                final int senderId = PreferenceManager.getDefaultSharedPreferences(SendMoneyActivity.this).getInt("userId", 0);
                                                JsonObject body = new JsonObject();
                                                body.addProperty("receiverUPI", editReceiverUpi.getText().toString().trim());
                                                body.addProperty("sendAmount", editSendMoneyAmount.getText().toString().trim());
                                                Log.e("SendMoneyActivity","body:    "+body);

                                                Ion.with(SendMoneyActivity.this)
                                                        .load("PUT", Utils.createUrl(Constants.API_SendMoney + "/" + senderId))
                                                        .setJsonObjectBody(body)
                                                        .asJsonObject()
                                                        .setCallback(new FutureCallback<JsonObject>() {
                                                            @Override
                                                            public void onCompleted(Exception e, JsonObject result) {
                                                                // System.out.print(result);
                                                                if (result.get("status").getAsString().equals(Constants.ERROR)) {
                                                                    showError(view, result.get("message").getAsString());
                                                                } else {
                                                                    JsonObject object = new JsonObject();
                                                                    //object.addProperty("userId",senderId);
                                                                    object.addProperty("receiverUPI",editReceiverUpi.getText().toString());
                                                                    object.addProperty("amount",editSendMoneyAmount.getText().toString());
                                                                    Log.e("SendMoneyActivity","Object:    "+object);

                                                                    Ion.with(SendMoneyActivity.this)
                                                                            .load("POST",Utils.createUrl(Constants.API_SendMoneyTransaction + "/"+ senderId))
                                                                            .setJsonObjectBody(object)
                                                                            .asJsonObject()
                                                                            .setCallback(new FutureCallback<JsonObject>() {
                                                                                @Override
                                                                                public void onCompleted(Exception e, JsonObject result) {
                                                                                    showSuccess(view,"WOWWWWWW");
                                                                                }
                                                                            });
                                                                    new FancyAlertDialog.Builder(SendMoneyActivity.this)
                                                                            .setTitle("Transaction Successful")
                                                                            .setMessage("Do another transaction?")
                                                                            .setBackgroundColor(Color.parseColor("#4CAF50"))  //Don't pass R.color.colorvalue
                                                                            .setNegativeBtnText("No")
                                                                            .setPositiveBtnBackground(Color.parseColor("#6193C7"))  //Don't pass R.color.colorvalue
                                                                            .setPositiveBtnText("Yes")
                                                                            .setNegativeBtnBackground(Color.parseColor("#403C3C"))  //Don't pass R.color.colorvalue
                                                                            .setAnimation(Animation.POP)
                                                                            .isCancellable(true)
                                                                            .setIcon(R.drawable.ic_correct, Icon.Visible)
                                                                            .OnPositiveClicked(new FancyAlertDialogListener() {
                                                                                @Override
                                                                                public void OnClick() {
                                                                                }
                                                                            })
                                                                            .OnNegativeClicked(new FancyAlertDialogListener() {
                                                                                @Override
                                                                                public void OnClick() {
                                                                                    finish();
                                                                                }
                                                                            })
                                                                            .build();
                                                                }
                                                            }
                                                        });
                                            }
                                        }
                                    });
                        }
                    }
                });
    }

    private void hideKeyboardPanel() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(buttonSendMoney.getWindowToken(), 0);

    }
}
