import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';

@Injectable()
export class ProductService {

    url = 'http://localhost:3000/product';

    constructor(private http: Http) { }

    public get() {
        return this.http.get(this.url);
    }

    public getDetails(id) {
        return this.http.get(this.url + '/' + id);
    }

    public post(title: string, shortDescription: string,
        longDescription: string, mrp: number, sellingPrice: number,
        category: number, imageFile: any) {

        const formData = new FormData();
        formData.append('title', title);
        formData.append('shortDescription', shortDescription);
        formData.append('longDescription', longDescription);
        formData.append('mrp', '' + mrp);
        formData.append('sellingPrice', '' + sellingPrice);
        formData.append('categoryId', '' + category);
        formData.append('photo', imageFile);

        return this.http.post(this.url, formData);
    }

    public put(title: string, shortDescription: string,
        longDescription: string, mrp: number, sellingPrice: number,
        category: number, productId: number) {

        const body = {
            title: title,
            shortDescription: shortDescription,
            longDescription: longDescription,
            mrp: mrp,
            sellingPrice: sellingPrice,
            categoryId: category
        };

        const headers = new Headers( { 'Content-Type': 'application/json' });
        const requestOptions = new RequestOptions({ headers: headers });

        return this.http.put(this.url + '/' + productId, body, requestOptions);
    }

    public delete(id) {
        return this.http.delete(this.url + '/' + id);
    }

}
