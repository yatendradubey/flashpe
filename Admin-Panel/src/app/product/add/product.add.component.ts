import { Component, OnInit } from '@angular/core';
import { CategoryService } from '../../category/category.service';
import { ProductService } from '../product.service';
import { Router } from '@angular/router';

@Component({
    selector: 'app-product-add',
    templateUrl: './product.add.component.html',
    styleUrls: ['./product.add.component.css']
})

export class ProductAddComponent implements OnInit {

    title = '';
    category = 1;
    shortDescription = '';
    longDescription = '';
    mrp = 0;
    sellingPrice = 0;
    selectedImage: object;

    // categories
    categories: object[];

    constructor(
        private router: Router,
        private productService: ProductService,
        private categoryService: CategoryService) { }

    ngOnInit() {
        this.categoryService
            .get()
            .subscribe( (response) => {
                const body = response.json();
                if (body.status === 'success') {
                    this.categories = body.data;
                }
            });
    }

    public onCancel() {
        this.router.navigate(['/product-list']);
    }

    public onSave() {
        this.productService
            .post(this.title, this.shortDescription, this.longDescription, this.mrp, this.sellingPrice, this.category, this.selectedImage)
            .subscribe( (response) => {
                this.router.navigate(['/product-list']);
            });
    }

    public onSelectImage(event) {
        this.selectedImage = event.target.files[0];
    }
}
