import { Component, OnInit } from '@angular/core';
import { CategoryService } from '../../category/category.service';
import { ProductService } from '../product.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-product-edit',
    templateUrl: './product.edit.component.html',
    styleUrls: ['./product.edit.component.css']
})

export class ProductEditComponent implements OnInit {

    title = '';
    category = 1;
    shortDescription = '';
    longDescription = '';
    mrp = 0;
    sellingPrice = 0;
    selectedImage: object;
    productId = 0;
    productThumbnail = '';

    // categories
    categories: object[];

    constructor(
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private productService: ProductService,
        private categoryService: CategoryService) { }

    ngOnInit() {
        this.categoryService
            .get()
            .subscribe( (response) => {
                const body = response.json();
                if (body.status === 'success') {
                    this.categories = body.data;
                }
            });

        this.activatedRoute
            .queryParams
            .subscribe((params) => {
                this.productId = params.id;
                this.getProductDetails();
            });
    }

    private getProductDetails() {
        this.productService
            .getDetails(this.productId)
            .subscribe( (response) => {
                const body = response.json();
                if (body.status === 'success') {
                    const product = body.data;
                    this.title = product.productTitle;
                    this.shortDescription = product.shortDescription;
                    this.longDescription = product.longDescription;
                    this.mrp = product.mrp;
                    this.sellingPrice = product.sellingPrice;
                    this.category = product.categoryId;
                    this.productThumbnail = product.thumbnail;
                }
            });
    }

    public onCancel() {
        this.router.navigate(['/product-list']);
    }

    public onSave() {
        console.log('category: ' + this.category);
        this.productService
            .put(this.title, this.shortDescription, this.longDescription, this.mrp, this.sellingPrice, this.category, this.productId)
            .subscribe( (response) => {
                this.router.navigate(['/product-list']);
            });
    }

    public onSelectImage(event) {
        this.selectedImage = event.target.files[0];
    }
}
