import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProductService } from './product.service';

@Component({
    selector: 'app-product-list',
    templateUrl: '/product.list.component.html',
    styleUrls: ['./product.list.component.css']
})

export class ProductListComponent implements OnInit {

    products: object[];

    constructor(
        private productService: ProductService,
        private router: Router) { }

    ngOnInit() {
        this.reloadProducts();
    }

    private reloadProducts() {
        this.productService
            .get()
            .subscribe( (response) => {
                const body = response.json();
                if (body.status === 'success') {
                    this.products = body.data;
                }
            });
    }

    public onDelete(product) {
        if (confirm('Are you sure you want to remove ' + product.title + '?')) {
            this.productService
                .delete(product.productId)
                .subscribe( (response) => {
                    this.reloadProducts();
                });
        }
    }

    public onEdit(product) {
        this.router.navigate(['/product-edit'], { queryParams: { id: product.productId } });
    }

    public onAdd() {
        this.router.navigate(['/product-add']);
    }
}
