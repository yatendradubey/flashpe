import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';

@Injectable()
export class AdminService implements CanActivate {

    private url = 'http://localhost:3000/admin/login';

    constructor(
        private router: Router,
        private http: Http) { }

    public login(userName: string, password: string) {
        const body = {
            userName: userName,
            password: password
        };

        const headers = new Headers( { 'Content-Type': 'application/json' });
        const requestOptions = new RequestOptions({ headers: headers });

        return this.http.post(this.url, body, requestOptions);
    }

    public canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        if (sessionStorage['loginStatus'] === '1') {
            return true;
        }

        this.router.navigate(['/login']);
        return false;
    }
}
