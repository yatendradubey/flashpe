import { Component, OnInit } from '@angular/core';
import { AdminService } from './admin.service';
import { Router } from '@angular/router';

@Component({
    selector: 'app-auth-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {
    userName = '';
    password = '';

    constructor(
        private router: Router,
        private service: AdminService) { }

    ngOnInit() { }

    onLogin() {
        if (this.userName.length === 0) {
            alert('enter user name');
        } else if (this.password.length === 0) {
            alert('enter password');
        } else {
            this.service
                .login(this.userName, this.password)
                .subscribe((response) => {
                    const body = response.json();
                    if (body.status === 'success') {
                        console.log(body);

                        // persist the login status
                        sessionStorage['loginStatus'] = 1;

                        // redirect to dashboard
                        this.router.navigate(['/customer-list']);
                    } else {
                        alert('invalid user name or password');
                    }
                });
        }
    }
}
