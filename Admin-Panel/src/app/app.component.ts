import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(private router: Router) {
  }

  public onLogout() {
    if (confirm('Are you sure you want to logout?')) {
      sessionStorage.removeItem('loginStatus');
      this.router.navigate(['/login']);
    }
  }
}
