import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ProductListComponent } from './product/product.list.component';
import { CategoryListComponent } from './category/category.list.component';
import { LoginComponent } from './authentication/login.component';
import { AdminService } from './authentication/admin.service';
import { CategoryService } from './category/category.service';
import { CategoryAddComponent } from './category/add/category.add.component';
import { CategoryEditComponent } from './category/edit/category.edit.component';
import { ProductAddComponent } from './product/add/product.add.component';
import { ProductService } from './product/product.service';
import { ProductEditComponent } from './product/edit/product.edit.component';
import { CustomerListComponent } from './customer/customer.list.component';
import { CustomerService } from './customer/customer.service';
import { OrderListComponent } from './order/order.list.component';
import { OrderService } from './order/order.service';
import { OrderEditComponent } from './order/edit/order.edit.component';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,

    ProductListComponent,
    ProductAddComponent,
    ProductEditComponent,

    CategoryListComponent,
    CategoryAddComponent,
    CategoryEditComponent,

    CustomerListComponent,

    OrderListComponent,
    OrderEditComponent,

    LoginComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    RouterModule.forRoot([
      { path: 'dashboard', component: DashboardComponent, canActivate: [AdminService] },

      { path: 'product-list', component: ProductListComponent, canActivate: [AdminService] },
      { path: 'product-add', component: ProductAddComponent, canActivate: [AdminService] },
      { path: 'product-edit', component: ProductEditComponent, canActivate: [AdminService] },

      { path: 'category-list', component: CategoryListComponent, canActivate: [AdminService] },
      { path: 'category-add', component: CategoryAddComponent, canActivate: [AdminService] },
      { path: 'category-edit', component: CategoryEditComponent, canActivate: [AdminService] },

      { path: 'customer-list', component: CustomerListComponent, canActivate: [AdminService] },

      { path: 'order-list', component: OrderListComponent, canActivate: [AdminService] },
      { path: 'order-edit', component: OrderEditComponent, canActivate: [AdminService] },

      { path: 'login', component: LoginComponent },

      // default
      { path: '**', component: LoginComponent }
    ])
  ],
  providers: [
    AdminService,
    CategoryService,
    ProductService,
    CustomerService,
    OrderService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
