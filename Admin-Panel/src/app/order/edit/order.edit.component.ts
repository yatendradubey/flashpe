import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { OrderService } from '../order.service';

@Component({
    selector: 'app-order-edit',
    templateUrl: './order.edit.component.html',
    styleUrls: ['./order.edit.component.css']
})

export class OrderEditComponent implements OnInit {
    orderId = 0;
    status = 1;
    customerId = 1;

    constructor(
        private orderService: OrderService,
        private activatedRoute: ActivatedRoute,
        private router: Router) { }

    ngOnInit() {
        this.activatedRoute
            .queryParams
            .subscribe( (params) => {
                this.orderId = params.id;
            });
    }

    changeStatus() {
        this.orderService
            .changeStatus(this.orderId, this.status, this.customerId)
            .subscribe((response) => {
                this.router.navigate(['/order-list']);
            });
    }
}
