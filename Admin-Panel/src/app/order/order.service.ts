import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import { SERVER_URL } from '../common/constants';

@Injectable()
export class OrderService {
    url = SERVER_URL + 'order';

    constructor(private http: Http) { }

    public get() {
        return this.http.get(this.url);
    }

    public changeStatus(orderId, status, customerId) {
        const body =  {
            status: status,
            customerId: customerId
        };

        const headers = new Headers( { 'Content-Type': 'application/json' });
        const requestOptions = new RequestOptions({ headers: headers });

        return this.http.put(this.url + '/status/' + orderId, body, requestOptions);
    }
}
