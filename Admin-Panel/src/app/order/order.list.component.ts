import { Component, OnInit } from '@angular/core';
import { OrderService } from './order.service';
import { Router } from '@angular/router';

@Component({
    selector: 'app-order-list',
    templateUrl: './order.list.component.html',
    styleUrls: ['./order.list.component.css']
})

export class OrderListComponent implements OnInit {
    orders: object[];

    constructor(
        private router: Router,
        private orderService: OrderService) { }

    ngOnInit() {
        this.loadOrders();
    }

    getStatusTitle(code) {
        // 1: open, 2: in processing, 3: processed, 4: dispatched, 5: out for delivery, 6: delivered, 7: cancelled
        let status = '';
        if (code === 1) {
            status = 'open';
        } else if (code === 2) {
            status = 'processing';
        } else if (code === 3) {
            status = 'processed';
        } else if (code === 4) {
            status = 'dispatched';
        } else if (code === 5) {
            status = 'out for delivery';
        } else if (code === 6) {
            status = 'delivered';
        } else if (code === 7) {
            status = 'cancelled';
        }

        return status;
    }

    onChange(order) {
        this.router.navigate(['/order-edit'], { queryParams: {id: order.id}});
    }

    private loadOrders() {
        this.orderService
            .get()
            .subscribe((response) => {
                const body = response.json();
                this.orders = body.data;
            });
    }
}
