import { Component, OnInit } from '@angular/core';
import { CustomerService } from './customer.service';

@Component({
    selector: 'app-customer-list',
    templateUrl: './customer.list.component.html',
    styleUrls: ['./customer.list.component.css']
})

export class CustomerListComponent implements OnInit {
    customers: object[];

    constructor(private customerService: CustomerService) { }

    ngOnInit() {
        this.loadCustomers();
    }

    public onChangeStatus(customer) {
     //   alert(customer.isActive);
        const status = customer.isActive === 0 ? 1 : 0;
     //   alert(status);
        this.customerService
            .changeStatus(customer.id, status)
            .subscribe((response) => {
                this.loadCustomers();
            });
    }

    loadCustomers() {
        this.customerService
            .get()
            .subscribe( (response) => {
                const body = response.json();
                console.log(body);
                this.customers = body.data;
            });
    }
}
