import { Injectable } from '@angular/core';
import { SERVER_URL } from '../common/constants';
import { Http, RequestOptions, Headers } from '@angular/http';

@Injectable()
export class CustomerService {

    url = SERVER_URL + 'user/getlist';

    constructor(private http: Http) {
    }

    public get() {
        return this.http.get(this.url);
    }

    public changeStatus(id: number, status: number) {
      //  alert((this.url + '/status/' + id);
        const body = {
            status: status
        };

        const headers = new Headers( { 'Content-Type': 'application/json' });
        const requestOptions = new RequestOptions({ headers: headers });

        return this.http.put(this.url + '/status/' + id, body, requestOptions);
    }
}
