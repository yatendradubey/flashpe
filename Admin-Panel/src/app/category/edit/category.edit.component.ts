import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CategoryService } from '../category.service';

@Component({
    selector: 'app-category-edit',
    templateUrl: './category.edit.component.html',
    styleUrls: ['./category.edit.component.css']
})

export class CategoryEditComponent implements OnInit {

    title = '';
    description = '';
    categoryId = 0;

    constructor(
        private activedRoute: ActivatedRoute,
        private service: CategoryService,
        private router: Router) { }

    // onCreate()
    ngOnInit() {
        this.activedRoute
            .queryParams
            .subscribe( (params) => {
                console.log(params);
                this.categoryId = params.id;
                this.loadCategoryDetails();
            });
    }

    private loadCategoryDetails() {
        this.service
            .getCategory(this.categoryId)
            .subscribe((response) => {
                const body = response.json();
                const data = body.data;
                this.title = data.title;
                this.description = data.description;
            });
    }

    public onCancel() {
        this.router.navigate(['/category-list']);
    }

    public onSave() {
        this.service
            .put(this.title, this.description, this.categoryId)
            .subscribe( (response) => {
                this.router.navigate(['/category-list']);
            });
    }
}
