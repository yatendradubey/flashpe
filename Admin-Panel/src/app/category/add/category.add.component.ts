import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CategoryService } from '../category.service';

@Component({
    selector: 'app-category-add',
    templateUrl: './category.add.component.html',
    styleUrls: ['./category.add.component.css']
})

export class CategoryAddComponent implements OnInit {

    title = '';
    description = '';

    constructor(
        private service: CategoryService,
        private router: Router) { }

    ngOnInit() { }

    public onCancel() {
        this.router.navigate(['/category-list']);
    }

    public onSave() {
        this.service
            .post(this.title, this.description)
            .subscribe( (response) => {
                this.router.navigate(['/category-list']);
            });
    }
}
