import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';

@Injectable()
export class CategoryService {

    private url = 'http://localhost:3000/category';

    constructor(private http: Http) { }

    public get() {
        return this.http.get(this.url);
    }

    public getCategory(id) {
        return this.http.get(this.url + '/' + id);
    }

    public post(title: string, description: string) {
        const body = {
            title: title,
            description: description
        };

        const headers = new Headers( { 'Content-Type': 'application/json' });
        const requestOptions = new RequestOptions({ headers: headers });

        return this.http.post(this.url, body, requestOptions);
    }

    public put(title: string, description: string, id: number) {
        const body = {
            title: title,
            description: description
        };

        const headers = new Headers( { 'Content-Type': 'application/json' });
        const requestOptions = new RequestOptions({ headers: headers });

        return this.http.put(this.url + '/' + id, body, requestOptions);
    }

    public delete(id) {
        return this.http.delete(this.url + '/' + id);
    }
}
