import { Component, OnInit } from '@angular/core';
import { CategoryService } from './category.service';
import { Router } from '@angular/router';

@Component({
    selector: 'app-category-list',
    templateUrl: './category.list.component.html',
    styleUrls: ['./category.list.component.css']
})

export class CategoryListComponent implements OnInit {
    public categories: object[];

    constructor(
        private router: Router,
        private categoryService: CategoryService) { }

    ngOnInit() {
        this.loadCategories();
    }

    private loadCategories() {
        this.categoryService
            .get()
            .subscribe((response) => {
                const body = response.json();
                if (body.status === 'success') {
                    this.categories = body.data;
                }
            });
    }

    public onAddCategory() {
        this.router.navigate(['/category-add']);
    }

    public onEdit(category) {
        this.router.navigate(['/category-edit'], { queryParams: { id: category.id }});
    }

    public onDelete(category) {
        this.categoryService
            .delete(category.id)
            .subscribe((response) => {
                const body = response.json();
                if (body.status === 'success') {
                    this.loadCategories();
                } else {
                    alert('error occurred while removing a category');
                }
            });
    }
}
